package INF101.lab2.pokemon;

import java.util.Random;

public class Pokemon {
    String name;
    int healthPoints;
    int maxHealthPoints;
    int strength;

    Pokemon(String name, int maxHealthPoints, int strength) {
        this.name = name;
        this.healthPoints = maxHealthPoints;
        this.maxHealthPoints = maxHealthPoints;
        this.strength = strength;
    }

    String getName() {
        return name;
    }

    int getStrength() {
        return strength;
    }

    int getCurrentHP() {
        return healthPoints;
    }

    int getMaxHP() {
        return maxHealthPoints;
    }

    boolean isAlive() {
        if (getCurrentHP() > 0) {
            return true;
        } else {
            return false;
        }
    }
    
    @Override
    public String toString() {
        return name + " HP: (" + healthPoints + "/" + maxHealthPoints + ") STR: " + strength;
    }

    void damage(int damageTaken) {
        if (damageTaken < 0) {
        damageTaken = 0;
        healthPoints = healthPoints - damageTaken;
        } else if (damageTaken > healthPoints) {
            damageTaken = healthPoints;
            healthPoints = healthPoints - damageTaken;
        } else {
        healthPoints = healthPoints - damageTaken;
        }
    }

    void attack(Pokemon target) {
        Random rand = new Random();
        int damageInflincted = (int) (rand.nextInt(this.strength+1));
        while (true) {
            if (damageInflincted < 0) {
                damageInflincted = (int) (rand.nextInt(this.strength+1));
            } else {
                break;
            }
        }
        System.out.println(this.name + " attacks " + target.name + ".");
        target.healthPoints = target.healthPoints - damageInflincted;
        if (target.healthPoints < 1) {
            target.healthPoints = 0;
            System.out.println(target.name + " takes " + damageInflincted + " damage and is left with " + target.healthPoints + "/" + target.maxHealthPoints + " HP");
            System.out.println(target.name + " is defeated by " + this.name + ".");
        } else {
            System.out.println(target.name + " takes " + damageInflincted + " damage and is left with " + target.healthPoints + "/" + target.maxHealthPoints + " HP");
        }
    }
}
