package INF101.lab2.pokemon;

// import static org.junit.jupiter.api.Assertions.assertNotNull;

public class Main {

    public static Pokemon pokemon1;
    public static Pokemon pokemon2;

    public static void main(String[] args) {
    
        if (pokemon1 == null || pokemon2 == null) {
            pokemon1 = new Pokemon("Pikachu", 94, 12);
            pokemon2 = new Pokemon("Oddish", 100, 3);
        }

        System.out.println(pokemon1.toString());
        System.out.println(pokemon2.toString() + "\n");

        while (true) {
            if (pokemon2.isAlive()) {
                pokemon1.attack(pokemon2);  
                if (pokemon2.healthPoints <= 0) {
                    break;
                }
            } else {
                break;
            }
            if (pokemon1.isAlive()) {
                pokemon2.attack(pokemon1);  
                if (pokemon1.healthPoints <= 0) {
                    break;
                }  
            } else {
                break;
            }
        }
    }
}
